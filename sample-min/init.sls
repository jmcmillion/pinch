# vim: sts=2 ts=2 sw=2 et ai ft=jinja
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula with context -%}

{{ formula.namespace }}.notify:
  test.show_notification:
    - text: Hello from "{{ formula.name }}" formula at path "{{ formula.path }}" ({{ formula.url }})
