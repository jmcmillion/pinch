# Pinch
A flexible, lightweight, and easy-to-use framework for creating Salt Formulas.

## Project Goals
- Make formulas easy to use for all Salt states.
- Avoid hard-coded references within the formula to itself.
- Separate configuration values from code.
- Minimize the use of conditional statements in state files.
- Define default configuration values and ways to override them.
- Provide a mechanism to create unique state identifiers.
- Allow a formula to be renamed or moved without breaking references.

## Overview

### Formula Configuration
All configurable values for a formula are defined in a set of YAML files, rather than hard-coded into the SLS files.
  | File name          | Required | Purpose                                  |
  | ------------------ | -------- | ---------------------------------------- |
  | `defaults.yaml`    | Yes      | Default values                           |
  | `osfamilymap.yaml` | No       | Overrides for operating system families  |
  | `osmap.yaml`      | No       | Overrides for specific operating systems |

In addition to the YAML files listed above, configuration values may also be provided by the following methods:
- Pillar data, under a top level-key matching the formula name.
- Custom grains, under the top level key `formula` and second level key matching the formula name.

### Accessing Configuration Values
Each SLS file and Jinja template must include one `import` line at the top of the file:
```
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula with context -%}
```

This one line produces a number of helpful effects:
- Detects the path of the current formula in the state.
- Merges the YAML configuration files, according to the minion operating system.
- Merges Pillar data, if present, to override the YAML configuration files.
- Yields a `formula` dictionary containing the appropriate configuration values for the minion operating system.
- Includes the following built-in keys/values in the `formula` dictionary:
	| Key                 | Description                                  | Example Value          |
	| ------------------- | -------------------------------------------- | ---------------------- |
	| `formula.path`      | The full formula path, separated by slashes. | `common/nginx`         |
	| `formula.namespace` | The full formula path, separated by dots.    | `common.nginx`         |
	| `formula.url`       | The URL to the formula in the state tree.    | `salt://common/nginx/` |
	| `formula.name`      | The last folder name in the formula path.    | `nginx`                |

## Setup
The only files needed to make all of this work are `formula.jinja` and `builtins.jinja` from the `shared` directory. The `import` line (above) will search the current directory and all ancestor paths until those files are found.

The recommended approach is to keep a single copy of these files at the root of the state tree. However, if a custom version is required by a particular formula, it is fine to keep the custom file in the formula directory, effectively overriding the shared files for a specific formula.

These files depend on several functions only available in the v3004 release of the `slsutil` module or newer. For sites running an older version of Salt, the v3004 `slsutil` module can be found in the source code and distributed via the `_modules` directory.

## Basic Usage
1) Copy the `sample-full` or `sample-min` directory into your state tree.
    - `sample-full` provides a full-featured example state file and file template.
    - `sample-min` provides a minimal example state file.
2) Rename it to something meaningful, like `apache` or `postgresql`.
3) Modify as needed.

## Tips

### Unique State Identifiers
Prefix every state identifier with `{{ formula.namespace }}`. This helps ensure that state identifiers in one formula do not conflict with those in another. This is preferable to using `{{ sls }}` for the same purpose, because `{{ sls }}` cannot be used to declare requisites between SLS files.

### Managed Files
To access the configuration dictionary from a managed file template, a context variable must be provided. This is done by including the `context` argument when invoking `file.managed` as follows. See `init.sls` in the `sample-full` directory for a complete example.
```
    - context:
        tplfile: {{ tplfile }}
```

## Advanced Usage

### Custom Dictionary Name
To use a dictionary name other than `formula`, simply provide another name in the import statement, using the `as` keyword.
```
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula as myformula with context -%}
```
Access the dictionary contents as usual, through the custom name.
```
{{ myformula.namespace }}.install:
  pkg.installed:
    - name: {{ myformula.package | yaml_encode }}
```

### Access Another Formula Dictionary
In some cases, it is necessary to access the configuration dictionary for another formula, or for several formulas within the same template.
By default, the import statement returns the configuration dictionary of the formula containing the current SLS file or template. This behavior can be overridden by setting the `formula_path` variable before the import statement.

The following example loads the configuration dictionary for the current formula into name `dashboard`, then loads the configuration dictionary for the NGINX formula into the name `nginx`.
```
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula as dashboard with context -%}

{%- set formula_path="common/nginx" -%}
{% from salt["slsutil.findup"](formula_path, "formula.jinja") import formula as nginx with context -%}
```
Note, in the example above, the use of `formula_path` as the starting point for the `slsutil.findup` function. This is not strictly necessary for an installation using a single, shared copy of `formula.jinja`. However, if the NGINX formula provided a customized version of that file, specifying the formula path as the starting point will permit it to be located.

### Access Multiple Formula Dictionaries
The technique above can be used multiple times within the same SLS file or template.
```
{% from salt["slsutil.findup"](tplfile, "formula.jinja") import formula as dashboard with context -%}

{%- set formula_path="common/nginx" -%}
{% from salt["slsutil.findup"](formula_path, "formula.jinja") import formula as nginx with context -%}

{%- set formula_path="colors/red" -%}
{% from salt["slsutil.findup"](formula_path, "formula.jinja") import formula as red with context -%}

{%- set formula_path="colors/blue" -%}
{% from salt["slsutil.findup"](formula_path, "formula.jinja") import formula as blue with context -%}
```
The example above will yield four configuration dictionaries: one from the current formula and three others.
